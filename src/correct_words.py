import os, sys;
import re;
import nltk;
import codecs;
import json;
import subprocess;
import fileinput;
import percepclassify as pc;
path = os.path.dirname(os.path.realpath(__file__))
query = {'its':1,'your':1,
        'their':1, 'loose':1,
         'to':1};
change= {"its":"it's","it's":"its", "your":"you're", "you're":"your",
         "their":"they're", "they're":"their", "loose":"lose", "lose":"loose",
          "to":"too","too":"to"}
file_name= {"its":"its","it's":"its", "your":"your", "you're":"your",
         "their":"their", "they're":"their", "loose":"loose", "lose":"loose",
          "to":"to","too":"to"}
object = {}
COUNT = 0;isCorrect  = [];
for name in query:
    file1 = open(path+"/"+name+'.weights', 'r');
    model = json.load(file1)
    object[name] = model;
def tokenize(doc):
    doc=re.sub(r"\b([,:;\"'\.\?/\{\}\[\]\(\)\~`!@#$%^&*\-=])(?!\b)",r" \1",doc);
    doc=re.sub(r"(?!\b)([,:;\"'\.\?/\{\}\[\]\(\)\~`!@#$%^&*\-=])\b",r" \1",doc);
    doc=re.sub(r"\b([,:;\"\?/\{\}\[\]\(\)\~`!@#$%^&*\-=])\b",r" \1 ",doc);
    return re.split(r"\s+",doc);
def checkTarget(tokes):    
    for i in range(len(tokes)):
        word = tokes[i].lower();
        if word in change: 
            return 1;
    return 0;

def replace(match): 
    global COUNT, isCorrect
    value =match.group(0);  
    value_i = value.lower();
    if value_i in change:
        if COUNT >= len(isCorrect):
           return value;
        if isCorrect[COUNT]== '0':
           COUNT+=1; 
           value_c = change[value_i];
           if value[0].isupper():
               value_c = list(value_c)   
               value_c[0] = value_c[0].upper()
               value_c = "".join(value_c)
           return value_c;
        else:
           COUNT+=1;
           return value;    
     
def main():
    global COUNT, isCorrect
    for line in fileinput.input():
         line = re.sub(r"[\r\n]+","",line);
         line_m = line.encode('ascii', 'ignore');
         tokes= tokenize(line_m);
         if not checkTarget(tokes): 
              print(line);continue; 
         isCorrect  = [];COUNT=0;
         tokes= nltk.pos_tag(tokes);
         for i in range(len(tokes)):
            output = ''; 
            word = tokes[i][0].lower();
            if word in change:
                name = file_name[word]
                output += word
                for j in range(-4,5):
                    if j==0: continue;
                    elif j<0:
                       if i+j<0: 
                          output += " pre"+str(abs(j))+"_word:##start##";
                          output += " pre"+str(abs(j))+"_tag:START";
                       else:
                          output += " pre"+str(abs(j))+"_word:"+tokes[i+j][0];
                          output += " pre"+str(abs(j))+"_tag:"+tokes[i+j][1];
                    elif j>0:
                       if i+j>=len(tokes): 
                          output += " post"+str(abs(j))+"_word:##end##";
                          output += " post"+str(abs(j))+"_tag:END";
                       else:
                          output += " post"+str(abs(j))+"_word:"+tokes[i+j][0];
                          output += " post"+str(abs(j))+"_tag:"+tokes[i+j][1];
                isCorrect.append(pc.isCorrect(object[name],output));
         pattern=r"\b(its|it's|their|they're|to|too|loose|lose|your|you're)\b"
         print(re.sub(pattern, replace , line, flags=re.I));

if __name__ == "__main__":
    main();
               