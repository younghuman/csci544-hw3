import sys;
import re;
import codecs;
file_name= {"its":"its","it's":"its", "your":"your", "you're":"your",
         "their":"their", "they're":"their", "loose":"loose", "lose":"loose",
          "to":"to","too":"to"}
query = {'its':1,'your':1,
        'their':1, 'loose':1,
         'to':1};
def tokenize(doc):
    doc=re.sub(r"\b([,:;\"'\.\?/\{\}\[\]\(\)\~`!@#$%^&*\-=])(?!\b)",r" \1",doc);
    doc=re.sub(r"(?!\b)([,:;\"'\.\?/\{\}\[\]\(\)\~`!@#$%^&*\-=])\b",r" \1",doc);
    doc=re.sub(r"\b([,:;\"\?/\{\}\[\]\(\)\~`!@#$%^&*\-=])\b",r" \1 ",doc);
    return re.split(r"\s+",doc);
def main():
    true_pos = {};
    correct_pos={};
    for name in query:
        true_pos[name] = 0;
        correct_pos[name]=0;
    file1 = codecs.open(sys.argv[1], 'r', "latin-1", errors="ignore")
    file2 = codecs.open(sys.argv[2], 'r', "latin-1", errors="ignore")
    for line in file1:
        line2 = file2.readline();
        line = re.sub(r"[\n\r]+","",line)
        line2= re.sub(r"[\n\r]+","",line2)
        tokes1 = tokenize(line)
        tokes2 = tokenize(line2)
        for i in range(len(tokes1)):
            if tokes1[i].lower() in file_name:
              value = tokes1[i].lower()
              true_pos[file_name[value]]+=1;
              if tokes1[i]==tokes2[i]:
                  correct_pos[file_name[value]]+=1;
    for name in query:
        #print(name+"    count:"+str(true_pos[name]))
        #print(name+"  correct:"+str(correct_pos[name]))
        print(name+" accuracy:"+str(float(correct_pos[name])/true_pos[name]))

if __name__ == "__main__":
    main();
    
    
    