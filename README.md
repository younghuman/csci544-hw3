#README
##Auto-correctify language errors in to/too, it's/its, lose/loose, your/you're, their/they're
###Brief Report

I extracted one version of *Wikipedia corpus* about 1.3G in .tgz format, extracted all the paragraphs containing one of these words, and 
used NLTK to do POS tagging. I extracted the 4 words and tags both before and after the current word 
as features and trained the model with the *Average Perceptron* classifier I wrote from hw1.
#######################
####After correcting the errors, the accuracy is:
their accuracy:0.991965973535
loose accuracy:0.910112359551
its accuracy:0.977926421405
to accuracy:0.977261916088
your accuracy:0.94289044289

####Which is much better than the original accuracy:
their accuracy:0.721172022684
loose accuracy:0.797752808989
its accuracy:0.727090301003
to accuracy:0.708543430692
your accuracy:0.702797202797

###Usage

`python src/correct_words.py < [input_file] > [output_file]`

###Third-party tools
NLTK package for python 2.7